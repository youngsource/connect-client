<?php

namespace Youngsource\Connect;

use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\UserProvider;
use Youngsource\Connect\Traits\ExternaUserProviderTrait;

final class ExternalAuthProvider implements UserProvider
{
    use ExternaUserProviderTrait;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => rtrim(config('connect.host'), '/')
        ]);
        $this->project = config('connect.project');
    }
}
