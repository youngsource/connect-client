<?php

namespace Youngsource\Connect\Traits;

/**
 * Trait ExternalAuthenticatableTrait
 * Trait implementaing the Authenticatable trait as to do it over the auth server of youngsource.
 *
 * @package Youngsource\Connect\Traits
 */
trait ExternalAuthenticatableTrait
{
    /** @var string */
    private $name;
    /** @var int */
    private $id;
    /** @var string */
    private $rememberToken;
    /** @var string */
    private $password;

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    final public function getAuthIdentifierName(): ?string
    {
        return $this->name;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return int
     */
    final public function getAuthIdentifier(): int
    {
        return $this->id;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    final public function getAuthPassword(): string
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string|null
     */
    final public function getRememberToken(): ?string
    {
        return $this->rememberToken;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string $value
     * @return void
     */
    final public function setRememberToken($value): void
    {
        $this->rememberToken = $value;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    final public function getRememberTokenName(): string
    {
        return 'remember_token';
    }
}
