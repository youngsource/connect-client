<?php

namespace Youngsource\Connect\Traits;

use GuzzleHttp\Client;

/**
 * Trait CanResetPasswordTrait
 *
 *
 * @package Youngsource\Connect\Traits
 */
trait CanResetPasswordTrait
{
    /**
     * Returns the mail address of the user.
     *
     * @return mixed
     */
    abstract public function getEmail(): string;

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset(): string
    {
        return $this->getEmail();
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token): void
    {
        $client = new Client([
            'base_uri' => rtrim(config('youngsource_login.host'), '/') . '/api/'
        ]);
        $project = config('youngsource_login.project');
        $client->post("password_reset/$project", compact('token'));
    }
}
