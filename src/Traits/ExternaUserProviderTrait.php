<?php

namespace Youngsource\Connect\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Contracts\Auth\Authenticatable;
use Psr\Http\Message\ResponseInterface;
use Youngsource\Connect\ExternalUser;

trait ExternaUserProviderTrait
{
    /** @var Client */
    private $client;
    /** @var string */
    private $project;

    /**
     * Retrieve a user by their unique identifier.
     *
     * @param  mixed $identifier
     * @return Authenticatable|null
     */
    final public function retrieveById($identifier): ?ExternalUser
    {
        return $this->receiveResult('receive_by_id', ['identifier' => $identifier]);
    }

    /**
     * Receives the result of the target login server endpoint.
     *
     * @param string $target
     * @param array $queryParams
     * @return ExternalUser|null
     */
    private function receiveResult(string $target, array $queryParams): ?ExternalUser
    {
        try {
            $response = $this->send($target, ['identifier' => $queryParams]);
        } catch (ClientException $exception) {
            return null;
        }
        if ($this->isValidResponse($response)) {
            return $this->packageResult($response);
        }
        return null;
    }

    /**
     * Sends a request to the target login server.
     *
     * @param string $target
     * @param array $options
     * @return ResponseInterface
     */
    private function send(string $target, array $options): ResponseInterface
    {
        return $this->client->post("$target/{$this->project}", $options);
    }

    /**
     * @param $response
     * @return bool
     */
    private function isValidResponse(ResponseInterface $response): bool
    {
        return $response->getStatusCode() === 200;
    }

    /**
     * @param ResponseInterface $response
     * @return ExternalUser
     */
    protected function packageResult(ResponseInterface $response): ExternalUser
    {
        $result = json_decode($response->getBody());
        return new ExternalUser(
            $result->name,
            $result->identifier,
            $result->password,
            $result->email,
            $result->remember_token
        );
    }

    /**
     * Retrieve a user by their unique identifier and "remember me" token.
     *
     * @param  mixed $identifier
     * @param  string $token
     * @return Authenticatable|null
     */
    final public function retrieveByToken($identifier, $token): ?Authenticatable
    {
        return $this->receiveResult('retrieve_by_token', [
            'identifier' => $identifier,
            'token' => $token,
        ]);
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param  Authenticatable $user
     * @param  string $token
     * @return void
     */
    final public function updateRememberToken(Authenticatable $user, $token): void
    {
        $user->setRememberToken($token);
    }

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array $credentials
     * @return Authenticatable|null
     */
    final public function retrieveByCredentials(array $credentials): ?Authenticatable
    {
        return $this->receiveResult('retrieveByCredentials', $credentials);
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param  Authenticatable $user
     * @param  array $credentials
     * @return bool
     */
    final public function validateCredentials(Authenticatable $user, array $credentials): bool
    {   /** @noinspection PhpUndefinedClassInspection */
        return \Hash::check($user->getAuthPassword(), $credentials['password']);
    }
}
