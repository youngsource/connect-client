<?php

namespace Youngsource\Connect;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

/**
 * Class AuthenticatingUserProvider
 */
final class ConnectServiceProvider extends ServiceProvider
{
    /**
     * Boots the youngsource login services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(UserProvider::class, ExternalAuthProvider::class);

        Auth::provider('youngsource-login', function() {
           return $this->app->make(UserProvider::class);
        });

        $this->publishes([__DIR__ . '/../config/connect.php' => config_path('connect')]);
    }
}
