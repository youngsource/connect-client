<?php

namespace Youngsource\Connect;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Notifications\Notifiable;
use Youngsource\Connect\Traits\CanResetPasswordTrait;
use Youngsource\Connect\Traits\ExternalAuthenticatableTrait;

/**
 * Class ExternalUser
 * @package App
 *
 */
class ExternalUser implements Authenticatable, CanResetPasswordContract
{
    use ExternalAuthenticatableTrait, CanResetPasswordTrait, Notifiable;

    /** @var string */
    private $email;

    /**
     * ExternalUser constructor.
     * @param string $name
     * @param int $id
     * @param string $password
     * @param string $email
     * @param null|string $rememberToken
     */
    public function __construct(string $name, int $id, string $password, string $email, ?string $rememberToken = null)
    {
        $this->name = $name;
        $this->id = $id;
        $this->password = $password;
        $this->email = $email;
    }

    /**
     * Returns the mail address of the user.
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }
}
