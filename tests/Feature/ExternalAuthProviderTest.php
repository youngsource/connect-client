<?php

namespace Tests\Feature;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use function GuzzleHttp\Psr7\stream_for;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Foundation\Application;
use function json_encode;
use Orchestra\Testbench\TestCase;
use ReflectionClass;
use Youngsource\Connect\ConnectServiceProvider;
use Youngsource\Connect\ExternalUser;

/**
 * Class ExternalUserProviderTest
 * @package Youngsource\Connect\Traits
 */
final class ExternalAuthProviderTest extends TestCase
{
    /** @var UserProvider */
    private $externalAuthProvider;
    /** @var ReflectionClass */
    private $reflectionExternalAuthProvider;

    protected function getPackageProviders($app): array
    {
        return [ ConnectServiceProvider::class ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getEnvironmentSetUp($app): void
    {
        parent::getEnvironmentSetUp($app);
        $app['config']->set('connect', [
            'project' => 'connect',
            'host' => 'https://connect.youngsource.be',
        ]);
    }

    /**
     * @throws \ReflectionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->externalAuthProvider = $this->app->make(UserProvider::class);
        $this->reflectionExternalAuthProvider = new \ReflectionClass($this->externalAuthProvider);
    }

    public function testReceiveByResultNegative(): void
    {
        $handler = HandlerStack::create(new MockHandler([ new Response(404) ]));
        $this->setProperty('client', new Client(['handler' => $handler]));
        $this->assertNull($this->externalAuthProvider->retrieveById(1));
    }

    public function testReceiveByResultPositive(): void
    {
        $properties = [
            'name' => 'testname',
            'identifier' => 1,
            'password' => \Hash::make('testpassword'),
            'email' => 'testemail',
            'remember_token' => 'testremembertoken',
        ];
        $handler = HandlerStack::create(new MockHandler([ new Response(200, [], json_encode($properties)) ]));
        $this->setProperty('client', new Client(['handler' => $handler]));
        $this->assertEquals(
            new ExternalUser(...array_values($properties)),
            $this->externalAuthProvider->retrieveById(1)
        );
    }

    private function setProperty(string $prop, $value): void
    {
        $property = $this->reflectionExternalAuthProvider->getProperty($prop);
        $property->setAccessible(true);
        $property->setValue($this->externalAuthProvider, $value);
    }
}
