<?php

return [
    'host' => 'https://auth.youngsource.be',
    'project' => env('APP_NAME', 'zero'),
];
